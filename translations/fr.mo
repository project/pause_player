��    C      4  Y   L      �  B   �     �  
          .        G     P     U     a     w     �     �  %   �     �     �  	   �     �  =        @     D     ]     t  *   �  2   �  +   �            V   4  G   �  
   �     �     �     �  	   �     �      	     	  !   	  F   A	     �	  7   �	  5   �	     �	     
     *
  *   7
     b
  B   h
  K   �
  :   �
  :   2     m     s     �     �     �     �     �     �     �     �     �  *        3  	   ?     I    O  F   f     �     �     �  A   �     "     6     ?     K     k  	   �     �  *   �     �     �  
   �     �  =        N  "   W  #   z     �  <   �  >   �  -   1     _     g  c   �  ?   �     '     9     F     R     Y     p     w  
   �  /   �  ]   �  
   ,  2   7  :   j     �     �     �  8   �     1  O   7  S   �  A   �  A     	   _  )   i  *   �     �     �     �     �     �                 8   8     q  	   �     �                              2   ?                 5   "   4      (   +       @   &   !           ,   C   )   =   %   >   	                 8   '                      *                 #          A      7            ;   <   
   :       1      0       6               3              -       9   .      /      $               B    Adds CSS classes on the HTML element container of the video player All the time All videos Appear mode At end (when end of the playing of all videos) Autoplay Both CSS classes Controls display mode Cover (no black bars) Debug Default Default (animation visible immediate) Default volume Description of the video Disappear Disappear mode Display mode of the image in the frame of the video container End End content display mode End content image path End content mode Endcontent displayed when player disappear Force ratio only for correct the size of the video HTML colors : blue, #4682B4, transparent... Height I have the commercial version If debug is activated, informations about the playing are added to the browser console If the value is empty, the title associated with the field will be used Image path Looping Mute Never No resize None None (no end content) On pause On play (launched by play button) On preload (player appear when preload of the first video is complete) On stop Path of an image that will be associated with the video Path of the image if the 'Web image' mode is selected Play button display Player background color Preload time Preload time before playback (in seconds). Ratio Sets a startup content that will be displayed before playing video Sets an ending content that will be displayed when the video playback stops Specify a number value and its unit. Ex : 270, 270px, 100% Specify a number value and its unit. Ex : 480, 480px, 100% Start Start content display mode Start content image path Start content mode Stretch Title of the video Video 1 Video 2 Video 3 Video 4 Video display mode Video display mode inside the video player Video image Web image Width Project-Id-Version: pause_player
POT-Creation-Date: 2018-01-05 18:51+0100
PO-Revision-Date: 2018-01-05 18:55+0100
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.5
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;t
X-Poedit-SearchPath-0: pause_player.module
X-Poedit-SearchPath-1: pause_player.install
X-Poedit-SearchPath-2: src/Plugin/Field/FieldFormatter/PausePlayerFormatter.php
 Ajoute des classes CSS à l'élément HTML conteneur du lecteur vidéo En tout temps Toutes les vidéos Mode d'apparition A la fin (lorsque la lecture de toutes les vidéos est terminée) Lecture automatique Les deux Classes CSS Mode d'affichage des contrôles Couvrir (sans bandes noires) Débogage Défaut Défaut (animation visible immédiatement) Volume par défaut Description de la vidéo Disparaît Mode de disparition Mode d'affichage de l'image dans le cadre du conteneur vidéo A la fin Mode d'affichage du contenu de fin Chemin de l'image du contenu de fin Mode du contenu de fin Le contenu de fin est affiché lorsque le lecteur disparaît Force le ration dans le but de corriger la taille de la vidéo Couleurs HTML : blue, #4682B4, transparent... Hauteur J'ai la version commerciale Si le débogage est activé, les informations de lecture sont ajoutées à la console du navigateur Si la valeur est vide, le titre associé au champ sera utilisé Chemin de l'image Répétition Rendre muet Jamais Sans redimensionnement Aucune Aucun (sans contenu de fin) A la pause A la lecture (lancée par le bouton de lecture) Au chargement (le lecteur apparaît lorsque le chargement de la première vidéo est complet) A l'arrêt Chemin d'une image qui sera associée à la vidéo Chemin de l'image si le mode 'Image Web' est sélectionné Affichage du bouton de lecture Couleur de fond du lecteur Temps de pré-chargement Temps de pré-chargement avant la lecture (en secondes). Ratio Définit un contenu de démarrage qui s'affichera avant la lecture de la vidéo Définit un contenu de fin qui s'affichera lorsque la lecture de la vidéo stoppera Spécifier une valeur nombre et son unité. Ex : 270, 270px, 100% Spécifier une valeur nombre et son unité. Ex : 480, 480px, 100% Au début Mode d'affichage du contenu de démarrage Chemin de l'image du contenu de démarrage Mode du contenu de démarrage Etirer Titre de la vidéo Vidéo 1 Vidéo 2 Vidéo 3 Vidéo 4 Mode d'affichage de la vidéo Mode d'affichage de la vidéo à l'intérieur du lecteur Image de la vidéo Image Web Largeur 